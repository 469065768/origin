//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 UiLib.rc 使用
//
#define IDB_BACKGROUND                  107
#define IDB_LINE                        108
#define IDB_CHECKBOX                    109
#define IDB_SLIDERTHUMB                 110
#define IDB_SLIDERBG                    111
#define IDB_TAB                         112
#define IDB_BTN                         113
#define IDB_COMBO                       114
#define IDB_CLOSE                       115
#define IDB_BTNNORMAL                   116
#define IDB_BTNPRESSED                  117
#define IDB_CHECKDISABLED               118
#define IDB_CHECKHOVER                  119
#define IDB_CHECKNORMAL                 120
#define IDB_CHECKSELECTED               121
#define IDB_CLOSENORMAL                 122
#define IDB_CLOSEPRESS                  123
#define IDB_RADIO                       124
#define IDB_UNRADIO                     125
#define IDB_BGEDIT                      126
#define IDB_BTNDISABLED                 127
#define IDB_BTNHOVER                    128
#define IDB_CLOSEHOVER                  129

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
