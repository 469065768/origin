#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_base = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<Label text=\"游戏加速\" textcolor=\"#FFD5CBAD\" width=\"65\" padding=\"30,15,0,5\"/>"
		L"<HorizontalLayout height=\"20\">"
		L"<Label text=\"移    速\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_move_speed_value\" text=\"50\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_move_speed\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"50\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"20\" >"
		L"<Label text=\"魔    速\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_magic_speed_value\" text=\"80\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_magic_speed\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"80\" />"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"20\" >"
		L"<Label text=\"攻    速\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_attack_speed_value\" text=\"20\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_attack_speed\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"20\" />"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"20\" >"
		L"<Label text=\"超    速\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_super_attack_value\" text=\"90\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_super_attack\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"90\" />"
		L"</HorizontalLayout>"
		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<Label text=\"变速齿轮\" textcolor=\"#FFD5CBAD\" width=\"65\" padding=\"30,5,0,5\"/>"
		L"<HorizontalLayout height=\"20\" >"
		L"<Label text=\"倍    数\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_acc_speed_value\" text=\"50\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_acc_speed\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"50\" />"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"20\" >"
		L"<Label text=\"微    调\" textcolor=\"#FFC5B181\" width=\"50\" padding=\"30,0,15,0\"/>"
		L"<Label name=\"base_micro_speed_value\" text=\"80\" textcolor=\"#FFC5B181\" width=\"20\" padding=\"0,0,15,0\"/>"
		L"<Slider name=\"base_slider_micro_speed\" width=\"300\" height=\"28\" min=\"0\" max=\"100\" value=\"80\" />"
		L"</HorizontalLayout>"
		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<HorizontalLayout height=\"39\" >"
		L"<HorizontalLayout padding=\"30,0,0,0\">"
		L"<CheckBox name=\"base_remove_tip_chkbox\" width=\"80\" text=\"去提示框\" padding=\"0,10,15,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_remove_trade_chkbox\" width=\"80\" text=\"去交易框\" padding=\"0,10,15,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_remove_npc_chkbox\" width=\"80\" text=\"去NPC框\" padding=\"0,10,15,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_speed_pick_chkbox\" width=\"80\" text=\"快速捡取\" padding=\"0,10,15,5\"/>"
		L"</HorizontalLayout>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"39\" >"
		L"<CheckBox name=\"base_lock_chkbox\" width=\"80\" text=\"锁定六格\" padding=\"30,5,15,10\"/>"
		L"</HorizontalLayout>"
		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<HorizontalLayout height=\"49\" >"
		L"<HorizontalLayout padding=\"30,0,0,0\">"
		L"<CheckBox name=\"base_case_dig_chkbox\" width=\"70\" text=\"防挖地\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_case_magic_chkbox\" width=\"70\" text=\"防魔法\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_case_sword_chkbox\" width=\"70\" text=\"防出刀\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_case_turn_around_chkbox\" width=\"70\" text=\"防转身\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"</HorizontalLayout>"
		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<HorizontalLayout height=\"49\" >"
		L"<HorizontalLayout padding=\"30,0,0,0\">"
		L"<CheckBox name=\"base_super_no_delay_chkbox\" width=\"80\" text=\"超级不卡\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_move_no_delay_chkbox\" width=\"80\" text=\"移动不卡\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_attack_no_delay_chkbox\" width=\"80\" text=\"攻击不卡\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<CheckBox name=\"base_magic_no_delay_chkbox\" width=\"80\" text=\"魔法不卡\" padding=\"0,10,15,10\"/>"
		L"</HorizontalLayout>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"
		L"</Window>";
}