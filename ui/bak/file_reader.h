#ifndef FILE_READER_59EAB6C9_6CEE_4770_9011_7CC5B87826D6_H__
#define FILE_READER_59EAB6C9_6CEE_4770_9011_7CC5B87826D6_H__

#include <xstring>
#include <tchar.h>

namespace Util
{

	/******************************************************************************/

	/**
	 * The class <code>file_reader</code>
	 *
	 */
	class	FileReader
	{
	public:
		FileReader(const std::wstring&	filepath);
		~FileReader();
	public:
		bool	open();
		int		read(TCHAR* buffer, size_t length);
		int		read(TCHAR* buffer);
		void	close();
		int		length();
	private:
		std::wstring	_filepath;		//文件路径
		bool			_ready;			//是否准备完毕
		FILE*			_file;			//文件指针
	};

}

/******************************************************************************/
#endif // FILE_READER_59EAB6C9_6CEE_4770_9011_7CC5B87826D6_H__
