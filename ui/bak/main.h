#include "../UIlib.h"

class MainWnd : public WindowImplBase
{
public:
	explicit MainWnd();

	LPCTSTR GetWindowClassName() const;
	CDuiString GetSkinFile();
	CDuiString GetSkinFolder();
	virtual CControlUI* CreateControl(LPCTSTR pstrClass);

	void InitWindow();
	void Notify(TNotifyUI& msg);

	LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	CPaintManagerUI* GetMainWndPaintManager();
protected:
private:
	CDuiString		m_strXMLPath;
	CButtonUI*		mpCloseBtn;
	CSliderUI*		mpMoveSpeed;
	CSliderUI*		mpMagicSpeed;
	CSliderUI*		mpAttackSpeed;
	CSliderUI*		mpSuperAttack;
	CSliderUI*		mpAccSpeed;
	CSliderUI*		mpMicroSpeed;
	CLabelUI*		mpMoveSpeedVal;
	CLabelUI*		mpMagicSpeedVal;
	CLabelUI*		mpAttackSpeedVal;
	CLabelUI*		mpSuperAttackVal;
	CLabelUI*		mpAccSpeedVal;
	CLabelUI*		mpMicroSpeedVal;

	COptionUI*		mpBase;
	COptionUI*		mpProtection;
	COptionUI*		mpJob;
	COptionUI*		mpAssist;
	COptionUI*		mpPick;
	COptionUI*		mpHangUp;

	COptionUI*		mpWarrior;
	COptionUI*		mpPriest;
	COptionUI*		mpTaoist;

	CTabLayoutUI*	mpMainTab;
	CTabLayoutUI*	mpJobTab;
};

extern "C" UILIB_API void show(HINSTANCE hInstance)
{
	CPaintManagerUI::SetInstance(hInstance);
	CPaintManagerUI::SetResourceDll(GetModuleHandle(L"UiLib.dll"));
	MainWnd *pFrame = new MainWnd();
	pFrame->Create(NULL, _T("test"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	pFrame->ShowModal();

	delete pFrame;
}