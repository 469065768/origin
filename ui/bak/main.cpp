#include "stdafx.h"

MainWnd::MainWnd()
{
	m_strXMLPath = Util::Config::g_main;
	/*TCHAR path[MAX_PATH];
	GetModuleFileName(CPaintManagerUI::GetResourceDll(), path, sizeof(path));
	path[_tcsrchr(path, L'\\') - path + 1] = 0;
	TCHAR dll[MAX_PATH];
	wsprintf(dll, L"%sUiLib.dllmain.xml", path);
	TCHAR buffer[15000];
	Util::FileReader* fr = new Util::FileReader(path);
	fr->open();
	fr->read(buffer);
	buffer[fr->length()] = '\0';
	m_strXMLPath = buffer;*/
}

LPCTSTR MainWnd::GetWindowClassName() const
{
	return _T("MainWnd");
}

CDuiString MainWnd::GetSkinFile()
{
	return m_strXMLPath;
}

CDuiString MainWnd::GetSkinFolder()
{
	return _T("");
}

CPaintManagerUI* MainWnd::GetMainWndPaintManager()
{
	return &m_PaintManager;
}

void MainWnd::InitWindow()
{
	mpCloseBtn = static_cast<CButtonUI*>(m_PaintManager.FindControl(_T("SysCloseBtn")));

	mpMoveSpeed = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_move_speed")));
	mpMagicSpeed = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_magic_speed")));
	mpAttackSpeed = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_attack_speed")));
	mpSuperAttack = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_super_attack")));
	mpAccSpeed = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_acc_speed")));
	mpMicroSpeed = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("slider_micro_speed")));

	mpMoveSpeedVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("move_speed_value")));
	mpMagicSpeedVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("magic_speed_value")));
	mpAttackSpeedVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("attack_speed_value")));
	mpSuperAttackVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("super_attack_value")));
	mpAccSpeedVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("acc_speed_value")));
	mpMicroSpeedVal = static_cast<CLabelUI*>(m_PaintManager.FindControl(_T("micro_speed_value")));

	mpBase = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("base")));
	mpProtection = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("protection")));
	mpJob = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("job")));
	mpAssist = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("assist")));
	mpPick = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("pick")));
	mpHangUp = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("hangup")));

	mpWarrior = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("warrior")));
	mpPriest = static_cast<COptionUI*>(m_PaintManager.FindControl(_T("priest")));

	mpMainTab = static_cast<CTabLayoutUI*>(m_PaintManager.FindControl(_T("main_tab")));
	mpJobTab = static_cast<CTabLayoutUI*>(m_PaintManager.FindControl(_T("job_tab")));
	CenterWindow();
}

void MainWnd::Notify(TNotifyUI& msg)
{
	if (msg.sType==L"click")
	{
		if (msg.pSender == mpCloseBtn)
		{
			Close(IDCLOSE);
		}
	}
	else if (msg.sType == DUI_MSGTYPE_VALUECHANGED)
	{
		TCHAR buffer[MAX_PATH];
		if (msg.pSender == mpMoveSpeed)
		{
			wsprintf(buffer, L"%d", mpMoveSpeed->GetValue());
			mpMoveSpeedVal->SetText(buffer);
		}
		else if (msg.pSender == mpMagicSpeed)
		{
			wsprintf(buffer, L"%d", mpMagicSpeed->GetValue());
			mpMagicSpeedVal->SetText(buffer);
		}
		else if (msg.pSender == mpAttackSpeed)
		{
			wsprintf(buffer, L"%d", mpAttackSpeed->GetValue());
			mpAttackSpeedVal->SetText(buffer);
		}
		else if (msg.pSender == mpSuperAttack)
		{
			wsprintf(buffer, L"%d", mpSuperAttack->GetValue());
			mpSuperAttackVal->SetText(buffer);
		}
		else if (msg.pSender == mpAccSpeed)
		{
			wsprintf(buffer, L"%d", mpAccSpeed->GetValue());
			mpAccSpeedVal->SetText(buffer);
		}
		else if (msg.pSender == mpMicroSpeed)
		{
			wsprintf(buffer, L"%d", mpMicroSpeed->GetValue());
			mpMicroSpeedVal->SetText(buffer);
		}
	}
	else if (msg.sType == DUI_MSGTYPE_SELECTCHANGED)
	{
		if (msg.pSender == mpBase)
		{
			mpMainTab->SelectItem(0);
		}
		else if (msg.pSender == mpProtection)
		{
			mpMainTab->SelectItem(1);
		}
		else if (msg.pSender == mpJob)
		{
			mpMainTab->SelectItem(2);
		}
		else if (msg.pSender == mpWarrior)
		{
			mpJobTab->SelectItem(0);
		}
		else if (msg.pSender == mpPriest)
		{
			mpJobTab->SelectItem(1);
		}
	}
	WindowImplBase::Notify(msg);
}

LRESULT MainWnd::HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = false;
	return 0;
}

CControlUI* MainWnd::CreateControl(LPCTSTR pstrClass)
{
	CDialogBuilder builder;
	if (!_tcsicmp(pstrClass, L"Base"))
	{
		return builder.Create(Util::Config::g_base.GetData(), _T("xml"), this, &m_PaintManager);
	}
	else if (!_tcsicmp(pstrClass, L"Protection"))
	{
		return builder.Create(Util::Config::g_protection.GetData(), _T("xml"), this, &m_PaintManager);
	}
	else if (!_tcsicmp(pstrClass, L"Job"))
	{
		return builder.Create(Util::Config::g_job.GetData(), _T("xml"), this, &m_PaintManager);
	}
	else if (!_tcsicmp(pstrClass, L"Warrior"))
	{
		return builder.Create(Util::Config::g_warrior.GetData(), _T("xml"), this, &m_PaintManager);
	}
	else if (!_tcsicmp(pstrClass, L"Priest"))
	{
		return builder.Create(Util::Config::g_priest.GetData(), _T("xml"), this, &m_PaintManager);
	}
	return NULL;
}