#ifndef CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__
#define CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>config</code> 
 *
 */
namespace Util
{
	class Config
	{
	public:
		static CDuiString g_main;
		static CDuiString g_base;
		static CDuiString g_protection;
		static CDuiString g_job;
		static CDuiString g_assist;


		static CDuiString g_warrior;
		static CDuiString g_priest;
		//static CDuiString g_taoist;

		//static CDuiString g_service;
	};
}

/******************************************************************************/
#endif // CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__
