#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_general = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<HorizontalLayout height=\"22\">"
		L"<Control width=\"10\"/>"
		L"<Option name=\"set_monster\" text=\"怪物设置\" width=\"65\"  autocalcwidth=\"true\" group=\"generalbar\" selected=\"true\" />"
		L"<Option name=\"set_humanoid\" text=\"人形怪设置\" width=\"80\"  autocalcwidth=\"true\" group=\"generalbar\" />"
		L"<Option name=\"set_player\" text=\"游戏玩家设置\" width=\"100\"  autocalcwidth=\"true\" group=\"generalbar\" />"
		L"<Option name=\"set_important\" text=\"重要设置\" width=\"65\"  autocalcwidth=\"true\" group=\"generalbar\" />"
		L"<Option name=\"set_expand\" text=\"扩展功能1\" width=\"70\"  autocalcwidth=\"true\" group=\"generalbar\" />"
		L"</HorizontalLayout>"
		L"<TabLayout name=\"general_tab\" height=\"405\" bordersize=\"1\" inset=\"1, 1, 1, 1\">"
		L"<SetMonster />"
		L"<SetHumanoid />"
		L"<SetPlayer />"
		L"<SetImportant />"
		L"<SetExpand />"
		L"</TabLayout >"
		L"</VerticalLayout >"
		L"</Window>";
}