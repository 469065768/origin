#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_set_monster = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<VerticalLayout height=\"90\" inset=\"30,0,5,0\">"
		L"<Label text=\"不攻击的怪物\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,5,0,5\"/>"
		L"<RichEdit name=\"monster_unattack_richedit\" multiline=\"true\" text=\"双头血魔|祖玛卫士#1000|祖玛雕像#900|虹魔教主%182|虹魔教主@200|名字不支持模糊匹配\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"92\" inset=\"30,0,5,0\">"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"monster_use_chkbox\" width=\"120\" text=\"遇到下列怪物使用:\" padding=\"0,5,5,5\"/>"
		L"<Combo name=\"monster_use_combo\" width=\"100\" height=\"22\" textpadding=\"4, 1, 1, 1\" padding=\"0,1,5,1\">"
		L"<ListLabelElement text=\"随机传送石\" selected=\"true\"/>"
		L"<ListLabelElement text=\"超级大补品\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"超级大补鸡\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"万年雪霜\" textcolor=\"#FFD5CBAD\"/>"
		L"</Combo>"
		L"<CheckBox name=\"monster_run_chkbox\" width=\"120\" text=\"遇到下列怪物就跑\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<RichEdit name=\"monster_run_richedit\" multiline=\"true\" text=\"双头血魔|祖玛卫士#1000|祖玛雕像#900|虹魔教主%182|虹魔教主@200|名字不支持模糊匹配\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<Label text=\"说明:\" textcolor=\"#FFD5CBAD\" padding=\"30,5,0,5\"/>"
		L"<Label text=\"1.怪物名称A|怪物名称B|怪物名称C|     比如 稻草|多钩|鹿|深林雪人|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"2.怪物名称A#血量|怪物名称B#血量|     比如 稻草#8000|假人#500|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"3.怪物名称A%外观值|怪物名称B%外观值| 比如 虹魔教主%182|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"4.怪物名称A@头顶显示图片值|          比如 虹魔教主@200|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"

		L"</VerticalLayout >"
		L"</Window>";
}