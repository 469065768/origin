#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_get_param = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"

		L"<VerticalLayout height=\"330\" inset=\"30,5,5,5\">"
		L"<Edit name=\"get_param_edit\" height=\"24\" text=\"\" padding=\"0,1,5,15\"/>"
		L"<HorizontalLayout >"
		L"<VerticalLayout width=\"130\" inset=\"0,1,5,1\">"
		L"<Button name=\"get_param_get_my_pos_btn\" text=\"获取自己坐标\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_open_npc_pos_btn\" text=\"获取打开NPC的坐标\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_get_npc_info_btn\" text=\"获取NPC详细信息\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_get_npc_infos_btn\" text=\"获取项NPC详细信息\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_get_all_goods_btn\" text=\"获取背包所有物品\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_get_all_equipments_btn\" text=\"获取背包所有装备\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_open_npc_name_btn\" text=\"获取打开NPC的名字\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_get_map_code_btn\" text=\"获取当前地图编号\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_exact_value_btn\" text=\"提取外观值\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"get_param_head_value_monster_btn\" text=\"头顶图片值怪物\" padding=\"0,1,5,1\"/>"
		L"</VerticalLayout >"
		L"<RichEdit name=\"get_param_richedit\" multiline=\"true\" text=\"\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}