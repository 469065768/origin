#include "StdAfx.h"


struct  DTIMsgHead
{
	enum DT_Head
	{
		DT_HEAD_LENGTH = 6,
	};

	virtual int EnCode(char* strData) = 0;
	virtual int DeCode(const char* strData) = 0;

	virtual uint16_t GetMsgID() const = 0;
	virtual void SetMsgID(uint16_t nMsgID) = 0;

	virtual uint32_t GetBodyLength() const = 0;
	virtual void SetBodyLength(uint32_t nLength) = 0;

	/*int64_t DT_HTONLL(int64_t nData)
	{
	return htonll(nData);
	}

	int64_t DT_NTOHLL(int64_t nData)
	{
	return ntohll(nData);
	}*/

	int32_t DT_HTONL(int32_t nData)
	{
		return htonl(nData);
	}

	int32_t DT_NTOHL(int32_t nData)
	{
		return ntohl(nData);
	}

	int16_t DT_HTONS(int16_t nData)
	{
		return htons(nData);
	}

	int16_t DT_NTOHS(int16_t nData)
	{
		return ntohs(nData);
	}

};
class DTCMsgHead : public DTIMsgHead
{
public:
	DTCMsgHead()
	{
		munSize = 0;
		munMsgID = 0;
	}

	// Message Head[ MsgID(2) | MsgSize(4) ]
	virtual int EnCode(char* strData)
	{
		uint32_t nOffset = 0;

		uint16_t nMsgID = DT_HTONS(munMsgID);
		memcpy(strData + nOffset, (void*)(&nMsgID), sizeof(munMsgID));
		nOffset += sizeof(munMsgID);

		uint32_t nPackSize = munSize + DT_HEAD_LENGTH;
		uint32_t nSize = DT_HTONL(nPackSize);
		memcpy(strData + nOffset, (void*)(&nSize), sizeof(munSize));
		nOffset += sizeof(munSize);

		if (nOffset != DT_HEAD_LENGTH)
		{
			assert(0);
		}

		return nOffset;
	}

	// Message Head[ MsgID(2) | MsgSize(4) ]
	virtual int DeCode(const char* strData)
	{
		uint32_t nOffset = 0;

		uint16_t nMsgID = 0;
		memcpy(&nMsgID, strData + nOffset, sizeof(munMsgID));
		munMsgID = DT_NTOHS(nMsgID);
		nOffset += sizeof(munMsgID);

		uint32_t nPackSize = 0;
		memcpy(&nPackSize, strData + nOffset, sizeof(munSize));
		munSize = DT_NTOHL(nPackSize) - DT_HEAD_LENGTH;
		nOffset += sizeof(munSize);

		if (nOffset != DT_HEAD_LENGTH)
		{
			assert(0);
		}

		return nOffset;
	}

	virtual uint16_t GetMsgID() const
	{
		return munMsgID;
	}
	virtual void SetMsgID(uint16_t nMsgID)
	{
		munMsgID = nMsgID;
	}

	virtual uint32_t GetBodyLength() const
	{
		return munSize;
	}
	virtual void SetBodyLength(uint32_t nLength)
	{
		munSize = nLength;
	}
protected:
	uint32_t munSize;
	uint16_t munMsgID;
};

void OperatorMain::InitWindow(CPaintManagerUI* mgr)
{
	mpCloseBtn = static_cast<CButtonUI*>(mgr->FindControl(_T("SysCloseBtn")));
	PTR_VOID(mpCloseBtn);
	mpSaveBtn = static_cast<CButtonUI*>(mgr->FindControl(_T("save_config_btn")));
	PTR_VOID(mpSaveBtn);
	mpReadBtn = static_cast<CButtonUI*>(mgr->FindControl(_T("read_config_btn")));
	PTR_VOID(mpReadBtn);
	mpBase = static_cast<COptionUI*>(mgr->FindControl(_T("base")));
	PTR_VOID(mpBase);
	mpProtection = static_cast<COptionUI*>(mgr->FindControl(_T("protection")));
	PTR_VOID(mpProtection);
	mpJob = static_cast<COptionUI*>(mgr->FindControl(_T("job")));
	PTR_VOID(mpJob);
	mpAssist = static_cast<COptionUI*>(mgr->FindControl(_T("assist")));
	PTR_VOID(mpAssist);
	mpPick = static_cast<COptionUI*>(mgr->FindControl(_T("pick")));
	PTR_VOID(mpPick);
	mpHangUp = static_cast<COptionUI*>(mgr->FindControl(_T("hangup")));
	PTR_VOID(mpHangUp);
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("main_tab")));
	PTR_VOID(mpTab);

	GetMAC(m_nMAC);
	GetLocalIP(m_nIP);
}

void OperatorMain::initCombo()
{

}

CControlUI* OperatorMain::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"Base"))
	{
		return builder.Create(Util::Config::g_base.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Protection"))
	{
		return builder.Create(Util::Config::g_protection.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Job"))
	{
		return builder.Create(Util::Config::g_job.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Assist"))
	{
		return builder.Create(Util::Config::g_assist.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Pick"))
	{
		return builder.Create(Util::Config::g_pick.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"HangUp"))
	{
		return builder.Create(Util::Config::g_hangup.GetData(), _T("xml"), pThis, mgr);
	}
	else
	{
		return NULL;
	}
}

bool OperatorMain::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpBase)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpProtection)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpJob)
	{
		mpTab->SelectItem(2);
	}
	else if (msg.pSender == mpAssist)
	{
		mpTab->SelectItem(3);
	}
	else if (msg.pSender == mpPick)
	{
		mpTab->SelectItem(4);
	}
	else if (msg.pSender == mpHangUp)
	{
		mpTab->SelectItem(5);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorMain::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorMain::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorMain::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	
	
	char body[MAX_PATH];
	DTCMsgHead content;
	content.SetMsgID(1001);
	
	if (msg.pSender == mpCloseBtn)
	{
		char buf[1024];
		sprintf(buf, "\nMAC[%s]\nip[%s]\n{you click close}\n", m_nMAC, m_nIP);
		LPSTR lpszContent = buf;
		content.SetBodyLength((int)strlen(lpszContent));
		int offset = content.EnCode(body);
		memcpy(body + offset, (void*)(lpszContent), strlen(lpszContent));
		int iLen = strlen(lpszContent) + offset;
		body[iLen++] = '\0';
		bool flag = client->Send((LPBYTE)body, iLen);
		if (!client->Stop())
		{
			//ASSERT(FALSE);
		}
		pThis->Close(IDCLOSE);
		return true;
	}
	else if (msg.pSender == mpSaveBtn)
	{
		char buf[1024];
		sprintf(buf, "\nMAC[%s]\nip[%s]\n{you click save}\n", m_nMAC, m_nIP);
		LPSTR lpszContent = buf;
		content.SetBodyLength((int)strlen(lpszContent));
		int offset = content.EnCode(body);
		memcpy(body + offset, (void*)(lpszContent), strlen(lpszContent));
		int iLen = strlen(lpszContent) + offset;
		body[iLen++] = '\0';
		bool flag = client->Send((LPBYTE)body, iLen);
		MessageBox(pThis->GetHWND(), L"你点击了保存", L"warning", MB_OK);
		return true;
	}
	else if (msg.pSender == mpReadBtn)
	{
		char buf[1024];
		sprintf(buf, "\nMAC[%s]\nip[%s]\n{you click read}\n", m_nMAC, m_nIP);
		LPSTR lpszContent = buf;
		content.SetBodyLength((int)strlen(lpszContent));
		int offset = content.EnCode(body);
		memcpy(body + offset, (void*)(lpszContent), strlen(lpszContent));
		int iLen = strlen(lpszContent) + offset;
		body[iLen++] = '\0';
		bool flag = client->Send((LPBYTE)body, iLen);
		MessageBox(pThis->GetHWND(), L"你点击了读取", L"warning", MB_OK);
		return true;
	}
	return false;
}