#include "../UIlib.h"
#include "socket/TcpClient.h"
#include "Global/helper.h"

class MainWnd : public WindowImplBase, public CTcpClientListener
{
public:
	explicit MainWnd();

	LPCTSTR GetWindowClassName() const;
	CDuiString GetSkinFile();
	CDuiString GetSkinFolder();
	virtual CControlUI* CreateControl(LPCTSTR pstrClass);

	void InitWindow();
	void Notify(TNotifyUI& msg);

	LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	CPaintManagerUI* GetMainWndPaintManager();

	//For socket
	virtual EnHandleResult OnSend(IClient* pClient, const BYTE* pData, int iLength){ return HR_IGNORE; }
	virtual EnHandleResult OnReceive(IClient* pClient, const BYTE* pData, int iLength){ return HR_IGNORE; }
	virtual EnHandleResult OnClose(IClient* pClient, EnSocketOperation enOperation, int iErrorCode){ return HR_IGNORE; }
	virtual EnHandleResult OnConnect(IClient* pClient){ return HR_IGNORE; }


protected:
private:
	CDuiString		m_strXMLPath;
	std::vector<IOperator*> m_nPageMrg;
	CTcpClient		m_Client;
	EnAppState		m_enState;
};

extern "C" UILIB_API void show(HINSTANCE hInstance)
{
	CPaintManagerUI::SetInstance(hInstance);
	CPaintManagerUI::SetResourceDll(GetModuleHandle(L"UiLib.dll"));
	MainWnd *pFrame = new MainWnd();
	pFrame->Create(NULL, _T("test"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	pFrame->ShowModal();

	delete pFrame;
}