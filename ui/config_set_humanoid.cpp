#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_set_humanoid = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<VerticalLayout height=\"90\" inset=\"30,0,5,0\">"
		L"<Label text=\"攻击人形怪\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,5,0,5\"/>"
		L"<RichEdit name=\"humanoid_attack_richedit\" multiline=\"true\" text=\"双头血魔|祖玛卫士#1000|祖玛雕像#900|虹魔教主%182|虹魔教主@200|名字不支持模糊匹配\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"90\" inset=\"30,0,5,0\">"
		L"<Label text=\"强制攻击的怪物\" textcolor=\"#FFD5CBAD\" width=\"90\" padding=\"0,5,0,5\"/>"
		L"<RichEdit name=\"humanoid_force_attack_richedit\" multiline=\"true\" text=\"双头血魔|祖玛卫士#1000|祖玛雕像#900|虹魔教主%182|虹魔教主@200|名字不支持模糊匹配\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<Label text=\"设置说明:\" textcolor=\"#FFD5CBAD\" padding=\"30,5,0,5\"/>"
		L"<Label text=\"1.怪物名称A|怪物名称B|怪物名称C|     比如 稻草|多钩|鹿|深林雪人|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"2.怪物名称A#血量|怪物名称B#血量|     比如 稻草#8000|假人#500|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"3.怪物名称A%外观值|怪物名称B%外观值| 比如 虹魔教主%182|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"4.怪物名称A@头顶显示图片值|          比如 虹魔教主@200|\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"

		L"</VerticalLayout >"
		L"</Window>";
}