#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_set_player = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<VerticalLayout height=\"90\" inset=\"30,0,5,0\">"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"player_run_chkbox\" width=\"80\" text=\"遇人就跑\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"player_fly_chkbox\" width=\"80\" text=\"遇人就飞\" padding=\"0,5,5,5\"/>"
		L"<Combo name=\"player_run_combo\" width=\"100\" height=\"22\" textpadding=\"4, 1, 1, 1\" padding=\"0,1,5,1\">"
		L"<ListLabelElement text=\"回城石\" selected=\"true\"/>"
		L"<ListLabelElement text=\"超级大补品\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"超级大补鸡\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"万年雪霜\" textcolor=\"#FFD5CBAD\"/>"
		L"</Combo>"
		L"</HorizontalLayout>"
		L"<RichEdit name=\"player_run_richedit\" multiline=\"true\" text=\"人物名称A|人物名称B|人物名称C|\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<Label text=\"填写玩家全名，则不飞（跑），不填写则见所有玩家都飞（跑）\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"注意：不支持姓名模糊匹配，必须填写全名。\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"
		L"<Label text=\"一般通常添加自己行会人，或者自己同地图挂机的小号。\" textcolor=\"#FFFF00FF\" padding=\"30,5,5,5\"/>"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout inset=\"30,0,5,0\">"
		L"<Label text=\"遇人就打\" textcolor=\"#FFD5CBAD\" width=\"90\" padding=\"0,5,0,5\"/>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"player_fight_chkbox\" width=\"80\" text=\"见人就打\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"遇到下列人物就攻击\" textcolor=\"#FFD5CBAD\" width=\"130\" padding=\"0,0,0,5\"/>"
		L"</HorizontalLayout>"
		L"<RichEdit name=\"player_fight_richedit\" multiline=\"true\" text=\"人物名称A|人物名称B|人物名称C|\"/>"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}