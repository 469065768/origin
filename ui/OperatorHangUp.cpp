#include "StdAfx.h"

void OperatorHangUp::InitWindow(CPaintManagerUI* mgr)
{
	mpScript = static_cast<COptionUI*>(mgr->FindControl(_T("script")));
	PTR_VOID(mpScript);
	mpGeneral = static_cast<COptionUI*>(mgr->FindControl(_T("general")));
	PTR_VOID(mpGeneral);
	mpNavigate = static_cast<COptionUI*>(mgr->FindControl(_T("navigate")));
	PTR_VOID(mpNavigate);
	mpGetParam = static_cast<COptionUI*>(mgr->FindControl(_T("get_param")));
	PTR_VOID(mpGetParam);
	mpHUJob = static_cast<COptionUI*>(mgr->FindControl(_T("hu_job")));
	PTR_VOID(mpHUJob);
	mpRecord = static_cast<COptionUI*>(mgr->FindControl(_T("record")));
	PTR_VOID(mpRecord);
	mpReloadProtection = static_cast<COptionUI*>(mgr->FindControl(_T("reload_protection")));
	PTR_VOID(mpReloadProtection);
	mpDebug = static_cast<COptionUI*>(mgr->FindControl(_T("debug")));
	PTR_VOID(mpDebug);
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("hangup_tab")));
	PTR_VOID(mpTab);
}

void OperatorHangUp::initCombo()
{

}

CControlUI* OperatorHangUp::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"Script"))
	{
		return builder.Create(Util::Config::g_script.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"General"))
	{
		return builder.Create(Util::Config::g_general.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Navigate"))
	{
		return builder.Create(Util::Config::g_navigate.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"GetParam"))
	{
		return builder.Create(Util::Config::g_get_param.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"HUJob"))
	{
		return builder.Create(Util::Config::g_hu_job.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Record"))
	{
		return builder.Create(Util::Config::g_record.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"ReloadProtection"))
	{
		return builder.Create(Util::Config::g_reload_protection.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Debug"))
	{
		return builder.Create(Util::Config::g_debug.GetData(), _T("xml"), pThis, mgr);
	}
	return NULL;
}

bool OperatorHangUp::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpScript)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpGeneral)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpNavigate)
	{
		mpTab->SelectItem(2);
	}
	else if (msg.pSender == mpGetParam)
	{
		mpTab->SelectItem(3);
	}
	else if (msg.pSender == mpHUJob)
	{
		mpTab->SelectItem(4);
	}
	else if (msg.pSender == mpRecord)
	{
		mpTab->SelectItem(5);
	}
	else if (msg.pSender == mpReloadProtection)
	{
		mpTab->SelectItem(6);
	}
	else if (msg.pSender == mpDebug)
	{
		mpTab->SelectItem(7);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorHangUp::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorHangUp::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorHangUp::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}