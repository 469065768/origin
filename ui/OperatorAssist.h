#ifndef OPERATORASSIST_EC390E84_BB0A_4359_A650_581C2571258D_H__
#define OPERATORASSIST_EC390E84_BB0A_4359_A650_581C2571258D_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorAssist</code> 
 *
 */
class OperatorAssist : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	COptionUI*		mpService;
	COptionUI*		mpDonate;
	COptionUI*		mpMask;

	CTabLayoutUI*	mpTab;
};

/******************************************************************************/
#endif // OPERATORASSIST_EC390E84_BB0A_4359_A650_581C2571258D_H__
