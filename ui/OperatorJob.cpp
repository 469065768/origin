#include "StdAfx.h"

void OperatorJob::InitWindow(CPaintManagerUI* mgr)
{
	//For option
	mpWarrior = static_cast<COptionUI*>(mgr->FindControl(_T("warrior")));
	PTR_VOID(mpWarrior);
	mpPriest = static_cast<COptionUI*>(mgr->FindControl(_T("priest")));
	PTR_VOID(mpPriest);
	mpTaoist = static_cast<COptionUI*>(mgr->FindControl(_T("taoist")));
	PTR_VOID(mpTaoist);

	//For tab
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("job_tab")));
	PTR_VOID(mpTab);

	//For combo
	mpHP1 = static_cast<CComboUI*>(mgr->FindControl(_T("protection_hp_1_combo")));
	PTR_VOID(mpHP1);
	mpHP1->RemoveAll();
	std::wstring data[] = { L"�󲹼�a", L"�󲹼�b", L"�󲹼�c", L"�󲹼�d" };
	for (int i = 0; i < 4; ++i)
	{
		CListLabelElementUI* node = new CListLabelElementUI;
		node->SetText(data[i].c_str());
		mpHP1->Add(node);
	}
	mpHP1->SetInternVisible(true);
	mpHP1->SelectItem(0);
}

void OperatorJob::initCombo()
{

}

CControlUI* OperatorJob::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"Warrior"))
	{
		return builder.Create(Util::Config::g_warrior.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Priest"))
	{
		return builder.Create(Util::Config::g_priest.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Taoist"))
	{
		return builder.Create(Util::Config::g_taoist.GetData(), _T("xml"), pThis, mgr);
	}
	return NULL;
}

bool OperatorJob::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpWarrior)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpPriest)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpTaoist)
	{
		mpTab->SelectItem(2);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorJob::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorJob::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorJob::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}