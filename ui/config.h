#ifndef CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__
#define CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>config</code> 
 *
 */
namespace Util
{
	class Config
	{
	public:
		//Tabs of main page
		static CDuiString g_main;
		static CDuiString g_base;
		static CDuiString g_protection;
		static CDuiString g_job;
		static CDuiString g_assist;
		static CDuiString g_pick;
		static CDuiString g_hangup;

		//Tabs of job page
		static CDuiString g_warrior;
		static CDuiString g_priest;
		static CDuiString g_taoist;

		//Tabs of assist page
		static CDuiString g_service;
		static CDuiString g_donate;
		static CDuiString g_mask;

		//Tabs of hangup page
		static CDuiString g_script;
		static CDuiString g_general;
		static CDuiString g_navigate;
		static CDuiString g_get_param;
		static CDuiString g_hu_job;
		static CDuiString g_record;
		static CDuiString g_reload_protection;
		static CDuiString g_debug;

		//Tabs of general page
		static CDuiString g_set_monster;
		static CDuiString g_set_humanoid;
		static CDuiString g_set_player;
		static CDuiString g_set_important;
		static CDuiString g_set_expand;

		//Tabs of reload protection page
		static CDuiString g_hu_protection;
		static CDuiString g_reload;
		static CDuiString g_masterwork;

	};
}

/******************************************************************************/
#endif // CONFIG_300FEC82_72C0_4B2D_8623_35D43FDA9C6D_H__
