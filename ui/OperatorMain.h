#ifndef OPERATORMAIN_60C30C55_D1B2_428A_898D_148378584FDD_H__
#define OPERATORMAIN_60C30C55_D1B2_428A_898D_148378584FDD_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorMain</code> 
 *
 */
class OperatorMain : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	CButtonUI*		mpCloseBtn;
	CButtonUI*		mpSaveBtn;
	CButtonUI*		mpReadBtn;

	COptionUI*		mpBase;
	COptionUI*		mpProtection;
	COptionUI*		mpJob;
	COptionUI*		mpAssist;
	COptionUI*		mpPick;
	COptionUI*		mpHangUp;

	CTabLayoutUI*	mpTab;

	char			m_nMAC[1024];
	char			m_nIP[1024];
};

/******************************************************************************/
#endif // OPERATORMAIN_60C30C55_D1B2_428A_898D_148378584FDD_H__
