#ifndef OPERATORJOB_F6F2CE6A_8975_4862_988A_E560A6900C92_H__
#define OPERATORJOB_F6F2CE6A_8975_4862_988A_E560A6900C92_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorJob</code> 
 *
 */
class OperatorJob : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	COptionUI*		mpWarrior;
	COptionUI*		mpPriest;
	COptionUI*		mpTaoist;

	CTabLayoutUI*	mpTab;

	CComboUI*		mpHP1;
};

/******************************************************************************/
#endif // OPERATORJOB_F6F2CE6A_8975_4862_988A_E560A6900C92_H__
