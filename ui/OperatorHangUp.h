#ifndef OPERATORHANGUP_626D9DBF_6DBE_4F5E_976D_D8E70A332115_H__
#define OPERATORHANGUP_626D9DBF_6DBE_4F5E_976D_D8E70A332115_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorHangUp</code> 
 *
 */
class OperatorHangUp : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	COptionUI*		mpScript;
	COptionUI*		mpGeneral;
	COptionUI*		mpNavigate;
	COptionUI*		mpGetParam;
	COptionUI*		mpHUJob;
	COptionUI*		mpRecord;
	COptionUI*		mpReloadProtection;
	COptionUI*		mpDebug;

	CTabLayoutUI*	mpTab;
};

/******************************************************************************/
#endif // OPERATORHANGUP_626D9DBF_6DBE_4F5E_976D_D8E70A332115_H__
