#ifndef OPERATORGENERAL_75B8EE2B_8BBE_4A87_AA98_43FB46446A9A_H__
#define OPERATORGENERAL_75B8EE2B_8BBE_4A87_AA98_43FB46446A9A_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorGeneral</code> 
 *
 */
class OperatorGeneral : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	COptionUI*		mpSetMonster;
	COptionUI*		mpSetHumanoid;
	COptionUI*		mpSetPlayer;
	COptionUI*		mpSetImportant;
	COptionUI*		mpSetExpand;

	CTabLayoutUI*	mpTab;
};

/******************************************************************************/
#endif // OPERATORGENERAL_75B8EE2B_8BBE_4A87_AA98_43FB46446A9A_H__
