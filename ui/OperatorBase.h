#ifndef OPERATORBASE_BF6F02D7_CE7C_4CE6_8C12_D4EF7F6D71A1_H__
#define OPERATORBASE_BF6F02D7_CE7C_4CE6_8C12_D4EF7F6D71A1_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorBase</code> 
 *
 */
class OperatorBase : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:

	CSliderUI*		mpMoveSpeed;
	CSliderUI*		mpMagicSpeed;
	CSliderUI*		mpAttackSpeed;
	CSliderUI*		mpSuperAttack;
	CSliderUI*		mpAccSpeed;
	CSliderUI*		mpMicroSpeed;
	CLabelUI*		mpMoveSpeedVal;
	CLabelUI*		mpMagicSpeedVal;
	CLabelUI*		mpAttackSpeedVal;
	CLabelUI*		mpSuperAttackVal;
	CLabelUI*		mpAccSpeedVal;
	CLabelUI*		mpMicroSpeedVal;
	CCheckBoxUI*	mpSuperNoDelay;
	CCheckBoxUI*	mpMoveNoDelay;
	CCheckBoxUI*	mpAttackNoDelay;
	CCheckBoxUI*	mpMagicNoDelay;
};

/******************************************************************************/
#endif // OPERATORBASE_BF6F02D7_CE7C_4CE6_8C12_D4EF7F6D71A1_H__
