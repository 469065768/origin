#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_job = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<HorizontalLayout height=\"22\">"
		L"<Control width=\"25\"/>"
		L"<Option name=\"warrior\" text=\"战士\" width=\"40\"  autocalcwidth=\"true\" group=\"jobbar\" selected=\"true\" />"
		L"<Option name=\"priest\" text=\"法师\" width=\"40\"  autocalcwidth=\"true\" group=\"jobbar\" />"
		L"<Option name=\"taoist\" text=\"道士\" width=\"40\"  autocalcwidth=\"true\" group=\"jobbar\" />"
		L"</HorizontalLayout>"
		L"<TabLayout name=\"job_tab\" height=\"405\" bordersize=\"1\" inset=\"1, 1, 1, 1\">"
		L"<Warrior />"
		L"<Priest />"
		L"<Taoist />"
		L"</TabLayout >"
		L"</VerticalLayout >"
		L"</Window>";
}