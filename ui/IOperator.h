#ifndef IOPERATE_F640F31E_1EEB_42B5_89B8_A8A37F03634B_H__
#define IOPERATE_F640F31E_1EEB_42B5_89B8_A8A37F03634B_H__

#include "../UIlib.h"
#include "socket/SocketInterface.h"

/******************************************************************************/

/**
 * The class <code>IOperator</code> 
 *
 */

#define PTR_NULL(p)				{ assert(p); if (0 == p) { return 0; } }
#define PTR_VOID(p)				{ assert(p); if (0 == p) { return; } }
#define STR_VOID(p)				{ assert(p); if (0 == p || 0 == _tcslen(p)) { return; } }
#define PTR_FALSE(p)			{ assert(p); if (0 == p) { return FALSE; } }

class IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr){}
	virtual void initCombo(){}
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr){ return NULL; }
	virtual bool SelectCtrl_event(TNotifyUI& msg){ return false; }
	//For combo
	virtual bool ItemSelectCtl_event(TNotifyUI& msg){ return false; }
	//For slide
	virtual bool ValueChangeCtl_event(TNotifyUI& msg){ return false; }
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL){ return false; }
};

/******************************************************************************/
#endif // IOPERATE_F640F31E_1EEB_42B5_89B8_A8A37F03634B_H__
