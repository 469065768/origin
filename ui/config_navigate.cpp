#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_navigate = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"

		L"<VerticalLayout height=\"34\" inset=\"30,5,5,5\">"
		L"<CheckBox name=\"navigate_auto_navigate_chkbox\" width=\"80\" text=\"自动巡航\" padding=\"0,1,0,1\"/>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"230\" inset=\"30,5,5,5\">"
		L"<Label text=\"使用线路巡航\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,1,0,1\"/>"
		L"<HorizontalLayout height=\"200\" >"
		L"<RichEdit name=\"navigate_use_path_navigate_richedit\" multiline=\"true\" width=\"150\" text=\"\"/>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<Edit name=\"navigate_use_path_navigate_edit\" height=\"24\" text=\"\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"navigate_use_path_navigate_get_my_pos_edit\" text=\"获取自己坐标\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"navigate_use_path_navigate_addin_path_edit\" text=\"添加到线路\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"navigate_use_path_navigate_del_path_edit\" text=\"删除线路\" padding=\"0,1,5,1\"/>"
		L"<Label text=\"说明:到挂机地图添加线路,这就是挂机\" textcolor=\"#FFFF00FF\" multiline=\"true\" padding=\"0,1,5,1\"/>"
		L"<Label text=\"巡航的线路!\" textcolor=\"#FFFF00FF\" multiline=\"true\" padding=\"0,1,5,1\"/>"
		L"</VerticalLayout >"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout inset=\"30,5,5,5\">"
		L"<Label text=\"使用物品巡航\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,1,0,1\"/>"
		L"<HorizontalLayout height=\"24\" >"
		L"<Label  text=\"挂机地图使用:\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,1,0,1\"/>"
		L"<Edit name=\"navigate_use_good_navigate_edit\" text=\"随机传送石\" padding=\"0,1,5,1\"/>"
		L"<Edit name=\"navigate_use_good_navigate_time_edit\" width=\"40\" text=\"3000\" padding=\"0,1,5,1\"/>"
		L"<Label text=\"毫秒\" textcolor=\"#FFD5CBAD\" width=\"24\" padding=\"0,1,0,1\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}