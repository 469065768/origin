#include "StdAfx.h"

namespace Util
{

	FileReader::FileReader(const std::wstring& filepath)
		:_filepath(filepath),
		_ready(false),
		_file(0)
	{

	}

	FileReader::~FileReader()
	{
		close();
	}

	bool	FileReader::open()
	{
		_ready = (_wfopen_s(&_file, _filepath.c_str(), L"rb") == 0);
		return	_ready;
	}

	int		FileReader::read(TCHAR *buffer, size_t length)
	{
		return _ready ? fread(buffer, 1, length, _file)
			: -1;
	}

	int		FileReader::read(TCHAR *buffer)
	{
		size_t len = length();
		return _ready ? fread(buffer, 1, len, _file)
			: -1;
	}

	void	FileReader::close()
	{
		if (_ready)
		{
			fclose(_file);
			_ready = false;
		}
	}

	int FileReader::length()
	{
		if (_ready)
		{
			fseek(_file, 0, SEEK_END);
			int lSize = ftell(_file);
			rewind(_file);
			return lSize;
		}
		return -1;
	}

}