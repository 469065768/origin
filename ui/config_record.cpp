#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_record = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"

		L"<VerticalLayout height=\"50\" inset=\"30,5,5,5\">"
		L"<Label text=\"出发脚本\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,1,0,1\"/>"
		L"<HorizontalLayout height=\"24\" >"
		L"<Button name=\"record_script_out_start_btn\" width=\"80\" text=\"开始录制\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"record_script_out_stop_btn\" width=\"80\" text=\"停止录制\" padding=\"0,1,5,1\"/>"
		L"<CheckBox name=\"record_script_out_identify_code_chkbox\" width=\"90\" text=\"有验证码:\" padding=\"0,1,0,1\"/>"
		L"<Button name=\"record_script_out_show_btn\" width=\"100\" text=\"显示到编辑区\" padding=\"0,1,5,1\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"50\" inset=\"30,5,5,5\">"
		L"<Label text=\"回收脚本\" textcolor=\"#FFD5CBAD\" width=\"80\" padding=\"0,1,0,1\"/>"
		L"<HorizontalLayout height=\"24\" >"
		L"<Button name=\"record_script_in_start_btn\" width=\"80\" text=\"开始录制\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"record_script_in_stop_btn\" width=\"80\" text=\"停止录制\" padding=\"0,1,5,1\"/>"
		L"<CheckBox name=\"record_script_in_identify_code_chkbox\" width=\"90\" text=\"有验证码:\" padding=\"0,1,0,1\"/>"
		L"<Button name=\"record_script_in_show_btn\" width=\"100\" text=\"显示到编辑区\" padding=\"0,1,5,1\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"200\" inset=\"30,5,5,5\">"
		L"<RichEdit name=\"record_richedit\" multiline=\"true\" text=\"\"/>"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}