#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_masterwork = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"

		L"<VerticalLayout height=\"34\" inset=\"30,5,5,5\">"
		L"<HorizontalLayout height=\"24\" >"
		L"<CheckBox name=\"masterwork_start_chkbox\" width=\"110\" text=\"启用小极品判断\" padding=\"0,1,0,1\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"40\" inset=\"30,5,5,5\">"
		L"<HorizontalLayout height=\"24\" >"
		L"<Label text=\"物品\" textcolor=\"#FFD5CBAD\" width=\"30\" padding=\"0,1,5,1\"/>"
		L"<Edit name=\"masterwork_good_edit\" width=\"60\" text=\"金手镯\" padding=\"0,1,5,1\"/>"
		L"<Label text=\"属性\" textcolor=\"#FFD5CBAD\" width=\"24\" padding=\"0,1,5,1\"/>"
		L"<Combo name=\"masterwork_good_combo\" width=\"100\" height=\"22\" textpadding=\"4, 1, 1, 1\" padding=\"0,1,5,1\">"
		L"<ListLabelElement text=\"疗伤药\" selected=\"true\"/>"
		L"<ListLabelElement text=\"超级大补品\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"超级大补鸡\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"万年雪霜\" textcolor=\"#FFD5CBAD\"/>"
		L"</Combo>"
		L"<Label text=\"大于等于\" textcolor=\"#FFD5CBAD\" width=\"50\" padding=\"0,1,5,1\"/>"
		L"<Edit name=\"masterwork_good_count_edit\" width=\"40\" text=\"3\" padding=\"0,1,5,1\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"200\" inset=\"30,5,5,5\">"
		L"<HorizontalLayout height=\"24\" >"
		L"<Button name=\"masterwork_clear_btn\" width=\"40\" text=\"清空\" padding=\"0,1,5,1\"/>"
		L"<Control  />"
		L"<Button name=\"masterwork_del_btn\" width=\"40\" text=\"删除\" padding=\"0,1,5,1\"/>"
		L"<Button name=\"masterwork_add_btn\" width=\"40\" text=\"添加\" padding=\"0,1,5,1\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout >"
		L"<RichEdit name=\"masterwork_richedit\" multiline=\"true\" text=\"\"/>"
		L"</HorizontalLayout >"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}