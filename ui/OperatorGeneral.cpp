#include "StdAfx.h"

void OperatorGeneral::InitWindow(CPaintManagerUI* mgr)
{
	mpSetMonster = static_cast<COptionUI*>(mgr->FindControl(_T("set_monster")));
	PTR_VOID(mpSetMonster);
	mpSetHumanoid = static_cast<COptionUI*>(mgr->FindControl(_T("set_humanoid")));
	PTR_VOID(mpSetHumanoid);
	mpSetPlayer = static_cast<COptionUI*>(mgr->FindControl(_T("set_player")));
	PTR_VOID(mpSetPlayer);
	mpSetImportant = static_cast<COptionUI*>(mgr->FindControl(_T("set_important")));
	PTR_VOID(mpSetImportant);
	mpSetExpand = static_cast<COptionUI*>(mgr->FindControl(_T("set_expand")));
	PTR_VOID(mpSetExpand);
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("general_tab")));
	PTR_VOID(mpTab);
}

void OperatorGeneral::initCombo()
{

}

CControlUI* OperatorGeneral::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"SetMonster"))
	{
		return builder.Create(Util::Config::g_set_monster.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"SetHumanoid"))
	{
		return builder.Create(Util::Config::g_set_humanoid.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"SetPlayer"))
	{
		return builder.Create(Util::Config::g_set_player.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"SetImportant"))
	{
		return builder.Create(Util::Config::g_set_important.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"SetExpand"))
	{
		return builder.Create(Util::Config::g_set_expand.GetData(), _T("xml"), pThis, mgr);
	}
	return NULL;
}

bool OperatorGeneral::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpSetMonster)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpSetHumanoid)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpSetPlayer)
	{
		mpTab->SelectItem(2);
	}
	else if (msg.pSender == mpSetImportant)
	{
		mpTab->SelectItem(3);
	}
	else if (msg.pSender == mpSetExpand)
	{
		mpTab->SelectItem(4);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorGeneral::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorGeneral::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorGeneral::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}