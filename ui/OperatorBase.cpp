#include "StdAfx.h"

void OperatorBase::InitWindow(CPaintManagerUI* mgr)
{
	//For slider
	mpMoveSpeed = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_move_speed")));
	PTR_VOID(mpMoveSpeed);
	mpMagicSpeed = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_magic_speed")));
	PTR_VOID(mpMagicSpeed);
	mpAttackSpeed = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_attack_speed")));
	PTR_VOID(mpAttackSpeed);
	mpSuperAttack = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_super_attack")));
	PTR_VOID(mpSuperAttack);
	mpAccSpeed = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_acc_speed")));
	PTR_VOID(mpAccSpeed);
	mpMicroSpeed = static_cast<CSliderUI*>(mgr->FindControl(_T("base_slider_micro_speed")));
	PTR_VOID(mpMicroSpeed);

	//For label
	mpMoveSpeedVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_move_speed_value")));
	PTR_VOID(mpMoveSpeedVal);
	mpMagicSpeedVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_magic_speed_value")));
	PTR_VOID(mpMagicSpeedVal);
	mpAttackSpeedVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_attack_speed_value")));
	PTR_VOID(mpAttackSpeedVal);
	mpSuperAttackVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_super_attack_value")));
	PTR_VOID(mpSuperAttackVal);
	mpAccSpeedVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_acc_speed_value")));
	PTR_VOID(mpAccSpeedVal);
	mpMicroSpeedVal = static_cast<CLabelUI*>(mgr->FindControl(_T("base_micro_speed_value")));
	PTR_VOID(mpMicroSpeedVal);

	//For checkbox
	mpSuperNoDelay = static_cast<CCheckBoxUI*>(mgr->FindControl(_T("base_super_no_delay_chkbox")));
	PTR_VOID(mpSuperNoDelay);
	mpSuperNoDelay->SetEnabled(false);
	mpMoveNoDelay = static_cast<CCheckBoxUI*>(mgr->FindControl(_T("base_move_no_delay_chkbox")));
	PTR_VOID(mpMoveNoDelay);
	mpMoveNoDelay->SetEnabled(false);
	mpAttackNoDelay = static_cast<CCheckBoxUI*>(mgr->FindControl(_T("base_attack_no_delay_chkbox")));
	PTR_VOID(mpAttackNoDelay);
	mpAttackNoDelay->SetEnabled(false);
	mpMagicNoDelay = static_cast<CCheckBoxUI*>(mgr->FindControl(_T("base_magic_no_delay_chkbox")));
	PTR_VOID(mpMagicNoDelay);
	mpMagicNoDelay->SetEnabled(false);
}

void OperatorBase::initCombo()
{

}

CControlUI* OperatorBase::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	return NULL;
}

bool OperatorBase::SelectCtrl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorBase::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorBase::ValueChangeCtl_event(TNotifyUI& msg)
{
	TCHAR buffer[MAX_PATH];
	if (msg.pSender == mpMoveSpeed)
	{
		wsprintf(buffer, L"%d", mpMoveSpeed->GetValue());
		mpMoveSpeedVal->SetText(buffer);
	}
	else if (msg.pSender == mpMagicSpeed)
	{
		wsprintf(buffer, L"%d", mpMagicSpeed->GetValue());
		mpMagicSpeedVal->SetText(buffer);
	}
	else if (msg.pSender == mpAttackSpeed)
	{
		wsprintf(buffer, L"%d", mpAttackSpeed->GetValue());
		mpAttackSpeedVal->SetText(buffer);
	}
	else if (msg.pSender == mpSuperAttack)
	{
		wsprintf(buffer, L"%d", mpSuperAttack->GetValue());
		mpSuperAttackVal->SetText(buffer);
	}
	else if (msg.pSender == mpAccSpeed)
	{
		wsprintf(buffer, L"%d", mpAccSpeed->GetValue());
		mpAccSpeedVal->SetText(buffer);
	}
	else if (msg.pSender == mpMicroSpeed)
	{
		wsprintf(buffer, L"%d", mpMicroSpeed->GetValue());
		mpMicroSpeedVal->SetText(buffer);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorBase::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}