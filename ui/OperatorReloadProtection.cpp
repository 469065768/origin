#include "StdAfx.h"

void OperatorReloadProtection::InitWindow(CPaintManagerUI* mgr)
{
	mpHUProtection = static_cast<COptionUI*>(mgr->FindControl(_T("hu_protection")));
	PTR_VOID(mpHUProtection);
	mpReload = static_cast<COptionUI*>(mgr->FindControl(_T("reload")));
	PTR_VOID(mpReload);
	mpMasterwork = static_cast<COptionUI*>(mgr->FindControl(_T("masterwork")));
	PTR_VOID(mpMasterwork);
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("reload_tab")));
	PTR_VOID(mpTab);
}

void OperatorReloadProtection::initCombo()
{

}

CControlUI* OperatorReloadProtection::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"HU_protection"))
	{
		return builder.Create(Util::Config::g_hu_protection.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Reload"))
	{
		return builder.Create(Util::Config::g_reload.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Masterwork"))
	{
		return builder.Create(Util::Config::g_masterwork.GetData(), _T("xml"), pThis, mgr);
	}
	return NULL;
}

bool OperatorReloadProtection::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpHUProtection)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpReload)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpMasterwork)
	{
		mpTab->SelectItem(2);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorReloadProtection::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorReloadProtection::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorReloadProtection::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}