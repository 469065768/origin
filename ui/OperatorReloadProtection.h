#ifndef OPERATORRELOADPROTECTION_B208AF2B_C4C6_4F1E_AD8E_FD6FB66BA687_H__
#define OPERATORRELOADPROTECTION_B208AF2B_C4C6_4F1E_AD8E_FD6FB66BA687_H__

#include "../UIlib.h"

/******************************************************************************/

/**
 * The class <code>OperatorReloadProtection</code> 
 *
 */
class OperatorReloadProtection : public IOperator
{
public:
	virtual void InitWindow(CPaintManagerUI* mgr);
	virtual void initCombo();
	virtual CControlUI* CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr);
	virtual bool SelectCtrl_event(TNotifyUI& msg);
	virtual bool ItemSelectCtl_event(TNotifyUI& msg);
	virtual bool ValueChangeCtl_event(TNotifyUI& msg);
	virtual bool ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client = NULL);

private:
	COptionUI*		mpHUProtection;
	COptionUI*		mpReload;
	COptionUI*		mpMasterwork;

	CTabLayoutUI*	mpTab;
};

/******************************************************************************/
#endif // OPERATORRELOADPROTECTION_B208AF2B_C4C6_4F1E_AD8E_FD6FB66BA687_H__
