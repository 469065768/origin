#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_warrior = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<VerticalLayout height=\"124\" inset=\"5,0,5,0\">"
		L"<Label text=\"基本\" textcolor=\"#FFD5CBAD\" width=\"25\" padding=\"30,5,0,5\"/>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_base_sep_kill_chkbox\" width=\"80\" text=\"隔位刺杀\" padding=\"30,5,5,5\"/>"
		L"<CheckBox name=\"warrior_base_auto_run_chkbox\" width=\"80\" text=\"智能跑位\" padding=\"0,5,5,5\"/>"
		L"<Edit name=\"warrior_base_auto_run_edit\" width=\"25\" text=\"2\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"跑位\" textcolor=\"#FFC5B181\" width=\"25\" padding=\"0,0,5,5\"/>"
		L"<CheckBox name=\"warrior_base_fire_chkbox\" width=\"80\" text=\"烈火近身\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_base_move_kill_chkbox\" width=\"80\" text=\"移位刺杀\" padding=\"30,5,5,5\"/>"
		L"<CheckBox name=\"warrior_base_attack_cruel_chkbox\" width=\"80\" text=\"攻击野蛮\" padding=\"0,5,5,5\"/>"
		L"<Edit name=\"warrior_base_attack_cruel_edit\" width=\"25\" text=\"3\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"野蛮\" textcolor=\"#FFC5B181\" width=\"25\" padding=\"0,0,5,5\"/>"
		L"<CheckBox name=\"warrior_base_step_chkbox\" width=\"80\" text=\"近身走位\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_base_lateral_pos_kill_chkbox\" width=\"80\" text=\"侧位刺杀\" padding=\"30,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"<Label height=\"2\" bkimage=\"res='108' restype='PNG'\" padding=\"5,5,5,5\"/>"
		L"<VerticalLayout height=\"200\" inset=\"5,0,5,0\">"
		L"<Label text=\"高级功能\" textcolor=\"#FFD5CBAD\" width=\"50\" padding=\"30,5,0,5\"/>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_senior_attack_chkbox\" width=\"80\" text=\"无影攻击\" padding=\"30,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_move_chkbox\" width=\"80\" text=\"无影移动\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_trade_chkbox\" width=\"100\" text=\"野蛮交易挑战\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_attack_dig_chkbox\" width=\"80\" text=\"攻击挖地\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_senior_cruel_chkbox\" width=\"80\" text=\"无影野蛮\" padding=\"30,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_overspeed_chkbox\" width=\"80\" text=\"过攻超速\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_unlimit_trade_chkbox\" width=\"100\" text=\"无限交易挑战\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_attack_turn_around_chkbox\" width=\"80\" text=\"攻击转身\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_senior_auto_beat_chkbox\" width=\"80\" text=\"自动反击\" padding=\"30,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_beat_cruel_chkbox\" width=\"80\" text=\"反击野蛮\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_orient_cruel_chkbox\" width=\"100\" text=\"定向野蛮\" padding=\"0,5,5,5\"/>"
		L"<CheckBox name=\"warrior_senior_reverse_cruel_chkbox\" width=\"80\" text=\"反向野蛮\" padding=\"0,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_senior_start_unlimit_sword_chkbox\" width=\"90\" text=\"启动无限刀\" padding=\"30,5,5,5\"/>"
		L"<Combo name=\"warrior_senior_start_unlimit_sword_combo\" width=\"100\" height=\"22\" textpadding=\"4, 1, 1, 1\" padding=\"0,5,5,0\">"
		L"<ListLabelElement text=\"Lv8无限刀\" selected=\"true\"/>"
		L"<ListLabelElement text=\"超级大补品\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"疗伤药\" textcolor=\"#FFD5CBAD\"/>"
		L"<ListLabelElement text=\"万年雪霜\" textcolor=\"#FFD5CBAD\"/>"
		L"</Combo>"
		L"<Edit name=\"warrior_senior_start_unlimit_sword_edit\" width=\"40\" text=\"100\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"毫秒\" textcolor=\"#FFD5CBAD\" width=\"24\" padding=\"0,0,5,0\"/>"
		L"<CheckBox name=\"warrior_senior_attack_over_chkbox\" width=\"80\" text=\"攻击过蓝\" padding=\"2,5,5,5\"/>"
		L"</HorizontalLayout>"
		L"<HorizontalLayout height=\"34\" >"
		L"<CheckBox name=\"warrior_senior_newest_multiplicity_attack_chkbox\" width=\"80\" text=\"多倍攻击\" padding=\"30,5,5,5\"/>"
		L"<Edit name=\"warrior_senior_newest_multiplicity_attack_edit\" width=\"40\" height=\"22\" text=\"2\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"倍\" textcolor=\"#FFD5CBAD\" width=\"25\" padding=\"0,0,5,0\"/>"
		L"<CheckBox name=\"warrior_senior_newest_multiplicity_magic_chkbox\" width=\"80\" text=\"多倍魔法\" padding=\"10,5,5,5\"/>"
		L"<Edit name=\"warrior_senior_newest_multiplicity_magic_edit\" width=\"40\" height=\"22\" text=\"2\" padding=\"0,5,5,5\"/>"
		L"<Label text=\"倍\" textcolor=\"#FFD5CBAD\" width=\"25\" padding=\"0,0,5,0\"/>"
		L"</HorizontalLayout>"
		L"</VerticalLayout >"

		L"</VerticalLayout >"
		L"</Window>";
}