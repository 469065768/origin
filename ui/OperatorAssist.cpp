#include "StdAfx.h"

void OperatorAssist::InitWindow(CPaintManagerUI* mgr)
{
	mpService = static_cast<COptionUI*>(mgr->FindControl(_T("service")));
	PTR_VOID(mpService);
	mpDonate = static_cast<COptionUI*>(mgr->FindControl(_T("donate")));
	PTR_VOID(mpDonate);
	mpMask = static_cast<COptionUI*>(mgr->FindControl(_T("mask")));
	PTR_VOID(mpMask);
	mpTab = static_cast<CTabLayoutUI*>(mgr->FindControl(_T("assist_tab")));
	PTR_VOID(mpTab);
}

void OperatorAssist::initCombo()
{

}

CControlUI* OperatorAssist::CreateControl(CDialogBuilder& builder, LPCTSTR pstrClass, IDialogBuilderCallback* pThis, CPaintManagerUI* mgr)
{
	if (!_tcsicmp(pstrClass, L"Service"))
	{
		return builder.Create(Util::Config::g_service.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Donate"))
	{
		return builder.Create(Util::Config::g_donate.GetData(), _T("xml"), pThis, mgr);
	}
	else if (!_tcsicmp(pstrClass, L"Mask"))
	{
		return builder.Create(Util::Config::g_mask.GetData(), _T("xml"), pThis, mgr);
	}
	return NULL;
}

bool OperatorAssist::SelectCtrl_event(TNotifyUI& msg)
{
	if (msg.pSender == mpService)
	{
		mpTab->SelectItem(0);
	}
	else if (msg.pSender == mpDonate)
	{
		mpTab->SelectItem(1);
	}
	else if (msg.pSender == mpMask)
	{
		mpTab->SelectItem(2);
	}
	else
	{
		return false;
	}
	return true;
}

bool OperatorAssist::ItemSelectCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorAssist::ValueChangeCtl_event(TNotifyUI& msg)
{
	return false;
}

bool OperatorAssist::ClickCtl_event(TNotifyUI& msg, CWindowWnd* pThis, ITcpClient* client)
{
	return false;
}