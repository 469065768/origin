#include "StdAfx.h"

namespace Util
{
	CDuiString Config::g_reload_protection = L"<?xml version=\"1.0\" encoding=\"utf - 8\" standalone=\"yes\" ?>"
		L"<Window>"
		L"<VerticalLayout inset=\"5,5,5,5\">"
		L"<HorizontalLayout height=\"22\">"
		L"<Control width=\"25\"/>"
		L"<Option name=\"hu_protection\" text=\"挂机保护\" width=\"80\"  autocalcwidth=\"true\" group=\"reloadprotectionbar\" selected=\"true\" />"
		L"<Option name=\"reload\" text=\"自动换装\" width=\"80\"  autocalcwidth=\"true\" group=\"reloadprotectionbar\" />"
		L"<Option name=\"masterwork\" text=\"极品识别\" width=\"80\"  autocalcwidth=\"true\" group=\"reloadprotectionbar\" />"
		L"</HorizontalLayout>"
		L"<TabLayout name=\"reload_tab\" height=\"405\" bordersize=\"1\" inset=\"1, 1, 1, 1\">"
		L"<HU_protection />"
		L"<Reload />"
		L"<Masterwork />"
		L"</TabLayout >"
		L"</VerticalLayout >"
		L"</Window>";
}