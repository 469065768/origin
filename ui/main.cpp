#include "stdafx.h"

MainWnd::MainWnd():
m_Client(this)
{
	m_strXMLPath = Util::Config::g_main;

	m_nPageMrg.clear();
	IOperator* mainPage = new OperatorMain();
	m_nPageMrg.emplace_back(mainPage);
	IOperator* basePage = new OperatorBase();
	m_nPageMrg.emplace_back(basePage);
	IOperator* jobPage = new OperatorJob();
	m_nPageMrg.emplace_back(jobPage);
	IOperator* assistPage = new OperatorAssist();
	m_nPageMrg.emplace_back(assistPage);
	IOperator* hangupPage = new OperatorHangUp();
	m_nPageMrg.emplace_back(hangupPage);
	IOperator* generalPage = new OperatorGeneral();
	m_nPageMrg.emplace_back(generalPage);
	IOperator* reloadProtectionPage = new OperatorReloadProtection();
	m_nPageMrg.emplace_back(reloadProtectionPage);
}

LPCTSTR MainWnd::GetWindowClassName() const
{
	return _T("MainWnd");
}

CDuiString MainWnd::GetSkinFile()
{
	return m_strXMLPath;
}

CDuiString MainWnd::GetSkinFolder()
{
	return _T("");
}

CPaintManagerUI* MainWnd::GetMainWndPaintManager()
{
	return &m_PaintManager;
}

void MainWnd::InitWindow()
{
	for each (auto& page in m_nPageMrg)
	{
		page->InitWindow(&m_PaintManager);
	}

	m_Client.SetSocketBufferSize(0x3FFFFF);
	m_enState = ST_STARTING;
	if (m_Client.Start(L"127.0.0.1", 5555, true))
	{

	}
	else
	{
		//::LogClientStartFail(m_Client.GetLastError(), m_Client.GetLastErrorDesc());
		m_enState = ST_STOPPED;
	}
	
	CenterWindow();
}

void MainWnd::Notify(TNotifyUI& msg)
{
	if (msg.sType==L"click")
	{
		for each (auto& page in m_nPageMrg)
		{
			if (page->ClickCtl_event(msg, this, &m_Client))return;
		}
	}
	else if (msg.sType == DUI_MSGTYPE_VALUECHANGED)
	{
		for each (auto& page in m_nPageMrg)
		{
			if (page->ValueChangeCtl_event(msg))return;
		}
	}
	else if (msg.sType == DUI_MSGTYPE_SELECTCHANGED)
	{
		for each (auto& page in m_nPageMrg)
		{
			if (page->SelectCtrl_event(msg))return;
		}
	}
	WindowImplBase::Notify(msg);
}

LRESULT MainWnd::HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = false;
	return 0;
}

CControlUI* MainWnd::CreateControl(LPCTSTR pstrClass)
{
	CDialogBuilder builder;
	for each (auto& page in m_nPageMrg)
	{
		CControlUI* ctrl = page->CreateControl(builder, pstrClass, this, &m_PaintManager);
		if (ctrl)return ctrl;
	}
	return NULL;
}