#include "stdafx.h"
#include "UIGroupBox.h"

namespace UiLib {
	CGroupBoxUI::CGroupBoxUI() :m_iFont(-1), m_bShowHtml(false)
	{

	}


	CGroupBoxUI::~CGroupBoxUI()
	{
	}

	///    @return LPCTSTR 返回控件类名
	///    @note    本函数返回控件类，格式为LPCTSTR
	LPCTSTR CGroupBoxUI::GetClass() const
	{
		return TEXT("GroupBoxUI");
	}


	/// @return LPVOID类型
	///    @note 获取接口
	LPVOID CGroupBoxUI::GetInterface(LPCTSTR pstrName)
	{
		if (_tcscmp(pstrName, TEXT("GroupBox")) == 0)
		{
			return static_cast<CGroupBoxUI*>(this);
		}

		return CContainerUI::GetInterface(pstrName);
	}


	///    设置控件属性
	/// @param pstrname        欲设置的属性名称，LPCTSTR类型
	/// @param pstrValue    欲设置的属性值，LPCTSTR类型
	/// @see                CControlUI::SetAttribute()
	/// @note                重载基类，增加部分基类没有的属性
	void CGroupBoxUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
	{
		if (_tcscmp(pstrName, _T("font")) == 0) SetFont(_ttoi(pstrValue));
		else if (_tcscmp(pstrName, _T("textcolor")) == 0)
		{
			if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
			LPTSTR pstr = NULL;
			DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
			SetTextColor(clrColor);
		}
		else if (_tcscmp(pstrName, _T("showhtml")) == 0)
			SetShowHtml(_tcscmp(pstrValue, _T("true")) == 0);

		else CControlUI::SetAttribute(pstrName, pstrValue);
	}

	/// 设置控件文字颜色
	/// @param    dwTextColor        欲设置的文字颜色
	/// @note    设置文字颜色，并立即刷新
	void CGroupBoxUI::SetTextColor(DWORD dwTextColor)
	{
		m_dwTextColor = dwTextColor;
		Invalidate();
	}


	/// 设置控件字体
	/// @param    iFont        欲设置的字体号
	/// @note    设置字体，并立即刷新
	void CGroupBoxUI::SetFont(int iFont)
	{
		m_iFont = iFont;
		Invalidate();
	}


	void CGroupBoxUI::SetShowHtml(bool bShowHtml)
	{
		if (m_bShowHtml == bShowHtml) return;

		m_bShowHtml = bShowHtml;
		Invalidate();
	}


	/// 关键部分
	void CGroupBoxUI::PaintText(HDC hDC)
	{
		//如果没有设置字体颜色，则用默认设置
		if (m_dwTextColor == 0)
		{
			m_dwTextColor = m_pManager->GetDefaultFontColor();
		}


		RECT rc;        //文字输出位置
		rc = GetPos();

		rc.left = rc.left + m_cxyBorderRound.cx + GROUPBOX_TEXT_OFFSET;    //这个地方采用了硬编码的方式，不知道怎么计算文字应该放的位置

		HFONT hOldFont = (HFONT)::SelectObject(hDC, m_pManager->GetFont(m_iFont));
		Gdiplus::Graphics graphics(hDC);
		Gdiplus::Font font(hDC);
		graphics.SetTextRenderingHint(Gdiplus::TextRenderingHintSystemDefault);
		graphics.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
		graphics.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
		Gdiplus::RectF rectF((Gdiplus::REAL)rc.left, (Gdiplus::REAL)rc.top, (Gdiplus::REAL)(rc.right - rc.left), (Gdiplus::REAL)(rc.bottom - rc.top));
		Gdiplus::SolidBrush brush(Gdiplus::Color(254, GetBValue(m_dwTextColor), GetGValue(m_dwTextColor), GetRValue(m_dwTextColor)));

		Gdiplus::StringFormat stringFormat = Gdiplus::StringFormat::GenericTypographic();
		Gdiplus::RectF bounds;
		graphics.MeasureString(m_sText, -1, &font, rectF, &stringFormat, &bounds);

		// MeasureString存在计算误差，这里加一像素
		rc.bottom = rc.top + (long)bounds.Height + 1;        //这两句是从UIRender.cpp中DrawText()中拷出来的，不知道意义何在
		rc.right = rc.left + (long)bounds.Width + 1;

		m_iTextWidth = (int)bounds.Width;
		m_iTextHeigh = (int)bounds.Height;

		graphics.DrawString(m_sText, -1, &font, rectF, &stringFormat, &brush);

		::SelectObject(hDC, hOldFont);
	}


	void CGroupBoxUI::PaintBorder(HDC hDC)
	{
		RECT rc = GetPos();        //画框框时的位置

		rc.top += (m_iTextHeigh * 1) / 2;        //最顶部的线移到Text的中下部

		int nSize = m_nBorderSize;
		//DWORD dwPenColor = m_dwBorderColor;
		Gdiplus::Graphics graphics(hDC);

		//消除锯齿
		graphics.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
		//const Gdiplus::Pen pen(Gdiplus::Color::Red, 1.0f);

		DWORD dwPenColor = GetAdjustColor(m_dwBorderColor);
		ASSERT(::GetObjectType(hDC) == OBJ_DC || ::GetObjectType(hDC) == OBJ_MEMDC);
		HPEN hPen = ::CreatePen(PS_SOLID | PS_INSIDEFRAME, nSize, RGB(GetBValue(dwPenColor), GetGValue(dwPenColor), GetRValue(dwPenColor)));
		HPEN hOldPen = (HPEN)::SelectObject(hDC, hPen);
		::SelectObject(hDC, ::GetStockObject(HOLLOW_BRUSH));

		//定位四个圆角的位置
		RECT rcTopLeftCorner = { rc.left, rc.top, rc.left + 2 * m_cxyBorderRound.cx, rc.top + 2 * m_cxyBorderRound.cy };
		RECT rcTopRightCorner = { rc.right - 2 * m_cxyBorderRound.cx, rc.top, rc.right, rc.top + 2 * m_cxyBorderRound.cy };
		RECT rcBottomLeftCorner = { rc.left, rc.bottom - 2 * m_cxyBorderRound.cy, rc.left + 2 * m_cxyBorderRound.cx, rc.bottom };
		RECT rcBottomRightCorner = { rc.right - 2 * m_cxyBorderRound.cx, rc.bottom - 2 * m_cxyBorderRound.cy, rc.right, rc.bottom };

		//画四个圆角
		const Gdiplus::Pen pen(Gdiplus::Color(GetBValue(dwPenColor), GetGValue(dwPenColor), GetRValue(dwPenColor)), (float)m_nBorderSize);
		graphics.DrawArc(&pen, rcTopLeftCorner.left, rcTopLeftCorner.top, rcTopLeftCorner.right - rcTopLeftCorner.left, rcTopLeftCorner.bottom - rcTopLeftCorner.top, 180.0f, 90.0f);//左上角
		graphics.DrawArc(&pen, rcTopRightCorner.left, rcTopRightCorner.top, rcTopRightCorner.right - rcTopRightCorner.left, rcTopRightCorner.bottom - rcTopRightCorner.top, 270.0f, 90.0f);//右上角
		graphics.DrawArc(&pen, rcBottomLeftCorner.left, rcBottomLeftCorner.top, rcBottomLeftCorner.right - rcBottomLeftCorner.left, rcBottomLeftCorner.bottom - rcBottomLeftCorner.top, 90.0f, 90.0f);//左下角
		graphics.DrawArc(&pen, rcBottomRightCorner.left, rcBottomRightCorner.top, rcBottomRightCorner.right - rcBottomRightCorner.left - 1, rcBottomRightCorner.bottom - rcBottomRightCorner.top - 1, 0.0f, 90.0f);//右下角


		//画线----GDI
		MoveToEx(hDC, rc.left, rc.top + m_cxyBorderRound.cy, NULL);            //左边线
		LineTo(hDC, rc.left, rc.bottom - m_cxyBorderRound.cy);

		int sep = 5;
		if (!_tcsicmp(m_sText, L""))sep = 0;
		MoveToEx(hDC, rc.left + m_cxyBorderRound.cx, rc.top, NULL);            //上第一条线
		LineTo(hDC, rc.left + m_cxyBorderRound.cx + GROUPBOX_TEXT_OFFSET - sep, rc.top);        //-5 是为了给Text增加左边间距

		MoveToEx(hDC, rc.left + m_cxyBorderRound.cx + GROUPBOX_TEXT_OFFSET + m_iTextWidth + sep, rc.top, NULL);        //上第二条线，+5是为了给Text增加右边间距
		LineTo(hDC, rc.right - m_cxyBorderRound.cx, rc.top);

		MoveToEx(hDC, rc.right-1, rc.top + m_cxyBorderRound.cy, NULL);        //右边线
		LineTo(hDC, rc.right-1, rc.bottom - m_cxyBorderRound.cy+1);

		MoveToEx(hDC, rc.left + m_cxyBorderRound.cx, rc.bottom, NULL);     //下边线
		LineTo(hDC, rc.right - m_cxyBorderRound.cx, rc.bottom);


		::SelectObject(hDC, hOldPen);
		::DeleteObject(hPen);
	}

	void CGroupBoxUI::SetPos(RECT rc)
	{
		CControlUI::SetPos(rc);
		rc = m_rcItem;

		// Adjust for inset
		rc.left += m_rcInset.left;
		rc.top += m_rcInset.top;
		rc.right -= m_rcInset.right;
		rc.bottom -= m_rcInset.bottom;
		if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible()) rc.right -= m_pVerticalScrollBar->GetFixedWidth();
		if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible()) rc.bottom -= m_pHorizontalScrollBar->GetFixedHeight();

		if (m_items.GetSize() == 0) {
			ProcessScrollBar(rc, 0, 0);
			return;
		}

		// Determine the minimum size
		SIZE szAvailable = { rc.right - rc.left, rc.bottom - rc.top };
		if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible())
			szAvailable.cx += m_pHorizontalScrollBar->GetScrollRange();

		int nAdjustables = 0;
		int cyFixed = 0;
		int nEstimateNum = 0;
		for (int it1 = 0; it1 < m_items.GetSize(); it1++) {
			CControlUI* pControl = static_cast<CControlUI*>(m_items[it1]);
			if (!pControl->IsVisible()) continue;
			if (pControl->IsFloat()) continue;
			SIZE sz = pControl->EstimateSize(szAvailable);
			if (sz.cy == 0) {
				nAdjustables++;
			}
			else {
				if (sz.cy < pControl->GetMinHeight()) sz.cy = pControl->GetMinHeight();
				if (sz.cy > pControl->GetMaxHeight()) sz.cy = pControl->GetMaxHeight();
			}
			cyFixed += sz.cy + pControl->GetPadding().top + pControl->GetPadding().bottom;
			nEstimateNum++;
		}
		cyFixed += (nEstimateNum - 1) * m_iChildPadding;

		// Place elements
		int cyNeeded = 0;
		int cyExpand = 0;
		if (nAdjustables > 0) cyExpand = MAX(0, (szAvailable.cy - cyFixed) / nAdjustables);
		// Position the elements
		SIZE szRemaining = szAvailable;
		int iPosY = rc.top + 15;
		if (m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible()) {
			iPosY -= m_pVerticalScrollBar->GetScrollPos();
		}
		int iPosX = rc.left + 10;
		if (m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible()) {
			iPosX -= m_pHorizontalScrollBar->GetScrollPos();
		}
		int iAdjustable = 0;
		int cyFixedRemaining = cyFixed;
		for (int it2 = 0; it2 < m_items.GetSize(); it2++) {
			CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
			if (!pControl->IsVisible()) continue;
			if (pControl->IsFloat()) {
				SetFloatPos(it2);
				continue;
			}

			RECT rcPadding = pControl->GetPadding();
			szRemaining.cy -= rcPadding.top;
			SIZE sz = pControl->EstimateSize(szRemaining);
			if (sz.cy == 0) {
				iAdjustable++;
				sz.cy = cyExpand;
				// Distribute remaining to last element (usually round-off left-overs)
				// 				if( iAdjustable == nAdjustables ) {
				// 					sz.cy = MAX(0, szRemaining.cy - rcPadding.bottom - cyFixedRemaining);
				// 				} 
				if (sz.cy < pControl->GetMinHeight()) sz.cy = pControl->GetMinHeight();
				if (sz.cy > pControl->GetMaxHeight()) sz.cy = pControl->GetMaxHeight();
			}
			else {
				if (sz.cy < pControl->GetMinHeight()) sz.cy = pControl->GetMinHeight();
				if (sz.cy > pControl->GetMaxHeight()) sz.cy = pControl->GetMaxHeight();
				//				cyFixedRemaining -= sz.cy + rcPadding.top + rcPadding.bottom;
			}

			//			cyFixedRemaining -= m_iChildPadding;

			sz.cx = pControl->GetFixedWidth();
			if (sz.cx == 0) sz.cx = szAvailable.cx - rcPadding.left - rcPadding.right;
			if (sz.cx < 0) sz.cx = 0;
			if (sz.cx < pControl->GetMinWidth()) sz.cx = pControl->GetMinWidth();
			if (sz.cx > pControl->GetMaxWidth()) sz.cx = pControl->GetMaxWidth();

			RECT rcCtrl = { iPosX + rcPadding.left, iPosY + rcPadding.top, iPosX + rcPadding.left + sz.cx, iPosY + sz.cy + rcPadding.top };
			pControl->SetPos(rcCtrl);

			iPosY += sz.cy + m_iChildPadding + rcPadding.top + rcPadding.bottom;
			cyNeeded += sz.cy + rcPadding.top + rcPadding.bottom;
			szRemaining.cy -= sz.cy + m_iChildPadding + rcPadding.bottom;
		}
		cyNeeded += (nEstimateNum - 1) * m_iChildPadding;

		// Process the scrollbar
		ProcessScrollBar(rc, 0, cyNeeded);
	}
}