#ifndef __UIDROPTARGET_H__
#define __UIDROPTARGET_H__

#pragma once

#include "../StdAfx.h"

namespace UiLib {
	class IDuiDropTarget
	{
	public:
		virtual   HRESULT OnDragEnter(IDataObject *pDataObj, DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect) = 0;
		virtual HRESULT  OnDragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) = 0;
		virtual HRESULT  OnDragLeave() = 0;
		virtual HRESULT  OnDrop(IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect) = 0;
	};
} // namespace UiLib

#endif // __UIDROPTARGET_H__